use chat::{chat_service_server::{ChatService, ChatServiceServer}, Message};
use tonic::{Request, Response, Status, transport::Server};
use gethostname::gethostname;
use std::net::SocketAddr;
use anyhow::Result;

pub mod chat {
    tonic::include_proto!("chat");
}

#[derive(Default)]
pub struct _ChatService {}

#[tonic::async_trait]
impl ChatService for _ChatService {
    async fn say_hello(&self, request: Request<Message>) -> std::result::Result<Response<Message>, Status> {
        let reply = Message {
            body: format!("Hello {}!", gethostname().to_str().unwrap()),
        };
        println!("Received: {:?}", request.into_inner().body);
        Ok(Response::new(reply))
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    
    let addr: SocketAddr = "0.0.0.0:3000".parse()?;
    let service = _ChatService::default();

    println!("Running....");
    Server::builder()
        .add_service(ChatServiceServer::new(service))
        .serve(addr)
        .await?;

    Ok(())
}
